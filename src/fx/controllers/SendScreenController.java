package fx.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import models.Operation;
import models.User;
import statics.Database;
import statics.FXMLHelper;
import statics.StaticContext;

import java.util.List;

public class SendScreenController {
    public TextField textFieldAmount;
    List<User> displayedUsers;
    @FXML
    private TableView<User> tableView;
    @FXML
    private TableColumn<User, String> columnName;
    @FXML
    private TableColumn<User, String> columnMoney;

    public void prerun() {
        displayedUsers = Database.selectAllUsers();
        columnName.setCellValueFactory(new PropertyValueFactory<User, String>("name"));
        columnMoney.setCellValueFactory(new PropertyValueFactory<User, String>("amount"));

        tableView.getItems().setAll(displayedUsers);
    }

    public void onButtonSendClick(ActionEvent actionEvent) {
        String text = textFieldAmount.getText();
        if (text.trim().length() == 0) {
            return;
        }
        int amount = Integer.parseInt(textFieldAmount.getText());
        if (amount > StaticContext.loginedUser.getAmount()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText("Not enough money");
            alert.setContentText("Abort");
            alert.showAndWait();
            return;
        }
        int selectedIndex = tableView.getSelectionModel().getSelectedIndex();
        if (selectedIndex == -1) {
            return;
        }
        User selectedUser = displayedUsers.get(selectedIndex);
        Operation operation = new Operation(0, amount, StaticContext.loginedUser.getId(), selectedUser.getId());
        Database.insertOperation(operation);

        //TODO: UPDATE AMOUNTS

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Success");
        alert.setHeaderText("Money Were Transfered");
        alert.setContentText("Thanks");
        alert.showAndWait();

        FXMLHelper.backScreen();
    }

    public void onButtonBackClick(ActionEvent actionEvent) {
        FXMLHelper.backScreen();
    }
}
