package fx.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import statics.FXMLHelper;
import statics.StaticContext;

public class UserScreenController {
    public Label labelAccountName;
    public Label labelTotalActiveMoney;

    public void prerun() {
        labelAccountName.setText(StaticContext.loginedUser.getName() + " " + StaticContext.loginedUser.getSurname().substring(0, 1) + ".");
        labelTotalActiveMoney.setText(String.format("%d р.", StaticContext.loginedUser.getAmount()));
    }

    public void onBackButtonClick(ActionEvent actionEvent) {
        FXMLHelper.backScreen();
    }

    public void onButtonSendClick(ActionEvent actionEvent) {
        SendScreenController sendScreen = FXMLHelper.loadScreenReturnController("SendScreen");
        sendScreen.prerun();
    }

    public void onButtonHistoryClick(ActionEvent actionEvent) {
        HistoryScreenController historyScreen = FXMLHelper.loadScreenReturnController("HistoryScreen");
        historyScreen.prerun();
    }
}
