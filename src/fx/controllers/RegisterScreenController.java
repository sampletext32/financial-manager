package fx.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import models.User;
import statics.Database;
import statics.FXMLHelper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class RegisterScreenController {
    public TextField textFieldLogin;
    public PasswordField passwordFieldPassword;
    public PasswordField passwordFieldRepeatPassword;
    public TextField textFieldName;
    public TextField textFieldSurname;

    public void onRegisterClick(ActionEvent actionEvent) {
        String login = textFieldLogin.getText();
        String password = passwordFieldPassword.getText();
        String repeatPassword = passwordFieldRepeatPassword.getText();
        String name = textFieldName.getText();
        String surname = textFieldSurname.getText();

        if (!password.equals(repeatPassword)) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Failed");
            alert.setHeaderText("Unable to register");
            alert.setContentText("Passwords you entered aren't equal.\nPlease Try Again.");
            alert.showAndWait();
            passwordFieldPassword.clear();
            passwordFieldRepeatPassword.clear();
            return;
        }
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");

            byte[] passwordMD5Bytes = md5.digest(password.getBytes());
            String passwordMD5String = new String(passwordMD5Bytes);

            User user = new User(0, login, passwordMD5String, name, surname, 10);
            Database.insertUser(user);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success");
            alert.setHeaderText("You were registered");
            alert.setContentText("You can now login under your account");
            alert.showAndWait();
            FXMLHelper.backScreen();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    public void onBackClicked(ActionEvent actionEvent) {
        FXMLHelper.backScreen();
    }
}
