package fx.controllers;

import javafx.event.ActionEvent;
import statics.FXMLHelper;

public class HelloScreenController {

    public void onLogInClick(ActionEvent actionEvent) {
        FXMLHelper.loadScreen("LogInScreen");
    }

    public void onRegisterClick(ActionEvent actionEvent) {
        FXMLHelper.loadScreen("RegisterScreen");
    }
}
