package fx.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import models.HistoryLine;
import models.Operation;
import statics.Database;
import statics.FXMLHelper;
import statics.StaticContext;

import java.util.List;
import java.util.stream.Collectors;

public class HistoryScreenController {

    @FXML
    private TableView<HistoryLine> tableView;
    @FXML
    private TableColumn<HistoryLine, Integer> columnId;
    @FXML
    private TableColumn<HistoryLine, String> columnSender;

    @FXML
    private TableColumn<HistoryLine, String> columnReceiver;

    @FXML
    private TableColumn<HistoryLine, Integer> columnAmount;

    public void prerun() {
        List<Operation> operations = Database.selectAllOperationsByUserId(StaticContext.loginedUser.getId());
        List<HistoryLine> lines = operations.stream().map(HistoryLine::new).collect(Collectors.toList());

        columnId.setCellValueFactory(new PropertyValueFactory<HistoryLine, Integer>("id"));
        columnSender.setCellValueFactory(new PropertyValueFactory<HistoryLine, String>("sender"));
        columnReceiver.setCellValueFactory(new PropertyValueFactory<HistoryLine, String>("receiver"));
        columnAmount.setCellValueFactory(new PropertyValueFactory<HistoryLine, Integer>("amount"));

        tableView.getItems().setAll(lines);
    }

    public void onButtonBackClick(ActionEvent actionEvent) {
        FXMLHelper.backScreen();
    }
}
