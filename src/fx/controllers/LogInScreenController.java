package fx.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import models.User;
import statics.Database;
import statics.FXMLHelper;
import statics.StaticContext;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LogInScreenController {
    public TextField textFieldLogin;
    public PasswordField passwordFieldPassword;

    public void onPasswordKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            performLogin();
        }
    }

    public void onLoginButtonClick(ActionEvent actionEvent) {
        performLogin();
    }

    private void performLogin() {
        String login = textFieldLogin.getText();
        String password = passwordFieldPassword.getText();

        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] passwordMD5Bytes = md5.digest(password.getBytes());
            String passwordMD5String = new String(passwordMD5Bytes);
            User user = Database.getUserByLoginAndPassword(login, passwordMD5String);
            if (user == null) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Login Failed");
                alert.setHeaderText("Unable to login");
                alert.setContentText("User or password were incorrect.\nPlease Try Again.");
                alert.showAndWait();
            } else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success");
                alert.setHeaderText("You were logined");
                alert.setContentText("Welcome");
                alert.showAndWait();
                StaticContext.loginedUser = user;
                UserScreenController userScreenController = FXMLHelper.loadScreenReturnController("UserScreen");
                userScreenController.prerun();
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        textFieldLogin.clear();
        passwordFieldPassword.clear();
    }

    public void onBackClicked(ActionEvent actionEvent) {
        FXMLHelper.backScreen();
    }
}
