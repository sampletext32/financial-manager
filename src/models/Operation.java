package models;

public class Operation {
    private int id;
    private int amount;
    private int sender_id;
    private int receiver_id;

    public Operation(int id, int amount, int sender_id, int receiver_id) {
        this.id = id;
        this.amount = amount;
        this.sender_id = sender_id;
        this.receiver_id = receiver_id;
    }

    public int getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getSender_id() {
        return sender_id;
    }

    public void setSender_id(int sender_id) {
        this.sender_id = sender_id;
    }

    public int getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(int receiver_id) {
        this.receiver_id = receiver_id;
    }
}
