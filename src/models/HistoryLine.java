package models;

import statics.Database;

public class HistoryLine {
    private Operation operation;
    private String sender;
    private String receiver;

    private int id;
    private int amount;

    public HistoryLine(Operation operation) {
        this.operation = operation;
        sender = Database.getUserById(operation.getSender_id()).getName();
        receiver = Database.getUserById(operation.getReceiver_id()).getName();
        amount = operation.getAmount();
        id = operation.getId();
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
