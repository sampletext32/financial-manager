package statics;

import models.Operation;
import models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Database {
    private static final String connectionString = "jdbc:mysql://localhost:3306/financial?serverTimezone=Europe/Moscow&useSSL=false&allowPublicKeyRetrieval=true";
    private static final String login = "root";
    private static final String password = "root";
    private static Connection connection;
    private static boolean initiated = false;

    public static List<User> selectAllUsers() {
        List<User> users = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users");
            User user = null;
            while (resultSet.next()) {
                int db_id = resultSet.getInt("id");
                String db_login = resultSet.getString("login");
                String db_password = resultSet.getString("password");
                String db_name = resultSet.getString("name");
                String db_surname = resultSet.getString("surname");
                int db_amount = resultSet.getInt("amount");
                user = new User(db_id, db_login, db_password, db_name, db_surname, db_amount);
                users.add(user);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public static User getUserByLoginAndPassword(String login, String password) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(String.format("SELECT * FROM users WHERE login='%s' AND password='%s' LIMIT 1", login, password));
            User user = null;
            if (resultSet.next()) {
                int db_id = resultSet.getInt("id");
                String db_login = resultSet.getString("login");
                String db_password = resultSet.getString("password");
                String db_name = resultSet.getString("name");
                String db_surname = resultSet.getString("surname");
                int db_amount = resultSet.getInt("amount");
                user = new User(db_id, db_login, db_password, db_name, db_surname, db_amount);
            }
            resultSet.close();
            statement.close();
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static User getUserById(int id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(String.format("SELECT * FROM users WHERE id='%d' LIMIT 1", id));
            User user = null;
            if (resultSet.next()) {
                int db_id = resultSet.getInt("id");
                String db_login = resultSet.getString("login");
                String db_password = resultSet.getString("password");
                String db_name = resultSet.getString("name");
                String db_surname = resultSet.getString("surname");
                int db_amount = resultSet.getInt("amount");
                user = new User(db_id, db_login, db_password, db_name, db_surname, db_amount);
            }
            resultSet.close();
            statement.close();
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void insertUser(User u) {
        try {
            Statement statement = connection.createStatement();
            statement.execute(String.format(
                    "INSERT INTO users (`login`, `password`, `name`, `surname`) VALUES ('%s', '%s', '%s', '%s')",
                    u.getLogin(), u.getPassword(), u.getName(), u.getSurname()));
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<Operation> selectAllOperationsByUserId(int user_id) {
        List<Operation> operations = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(String.format("SELECT * FROM operations WHERE sender_id='%d' OR receiver_id='%d'", user_id, user_id));
            Operation operation = null;
            while (resultSet.next()) {
                int db_id = resultSet.getInt("id");
                int db_sender_id = resultSet.getInt("sender_id");
                int db_receiver_id = resultSet.getInt("receiver_id");
                int db_amount = resultSet.getInt("amount");
                operation = new Operation(db_id, db_amount, db_sender_id, db_receiver_id);
                operations.add(operation);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return operations;
    }

    public static void connect() {
        try {
            connection = DriverManager.getConnection(connectionString, login, password);
            initiated = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void close() {
        if (initiated) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isInitiated() {
        return initiated;
    }

    public static void insertOperation(Operation operation) {
        try {
            Statement statement = connection.createStatement();
            statement.execute(String.format(
                    "INSERT INTO operations (`amount`, `sender_id`, `receiver_id`) VALUES ('%d', '%d', '%d')", operation.getAmount(), operation.getSender_id(), operation.getReceiver_id()));
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
